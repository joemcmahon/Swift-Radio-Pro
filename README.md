#Swift Radio

Swift Radio is an open source radio station app with robust and professional features. This is a fully realized Radio App built entirely in Swift. This is the AVPlayer branch, it is now updated to Swift 3.

![alt text](http://matthewfecher.com/wp-content/uploads/2015/09/screen-1.jpg "Swift Radio")

##Important Notes
Please refer to the [master readme](https://github.com/swiftcodex/Swift-Radio-Pro/blob/master/README.md) for the latest news, notes, and instructions
